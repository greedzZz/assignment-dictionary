ASM        = nasm
ASMFLAGS   =-felf64 -g

main.o: main.asm lib.o dict.o words.inc colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o dict.o lib.o
	ld -o $@ $^
