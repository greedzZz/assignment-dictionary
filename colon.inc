%define PREV 0

%macro colon 2
    %2:
        dq PREV
        db %1, 0
    %define PREV %2
%endmacro
