%include "lib.inc"

global find_word
section .text

find_word:
.loop:
    cmp rsi, 0
    je .end
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .end
    mov rsi, [rsi]
    jmp .loop
.end:
    mov rax, rsi
    ret
