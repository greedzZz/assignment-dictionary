global exit
global string_length
global print_string
global print_error
global print
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, 1
    jmp print

print_error:
    mov rsi, 2
    jmp print

print:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    dec rsp
    mov byte [rsp], 0
    xor r9, r9
.loop:
    xor rdx, rdx
    div r8
    add dl, 0x30
    dec rsp
    mov [rsp], dl
    inc r9
    cmp rax, 0
    jne .loop
    mov rdi, rsp
    push r9
    call print_string
    pop r9
    add rsp, r9
    inc rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .unsigned
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.unsigned:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
.loop:
    mov r9b, [rdi+r8]
    mov r10b, [rsi+r8]
    cmp r9b, r10b
    jne .no
    cmp r9b, 0
    je .yes
    inc r8
    jmp .loop
.yes:
    mov rax, 1
    ret
.no:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    push byte 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r8, r8
.space:
    push rdi
    push rsi
    push r8
    call read_char
    pop r8
    pop rsi
    pop rdi
    cmp rax, 0x20
    je .space
    cmp rax, 0x9
    je .space
    cmp rax, 0xA
    je .space
    cmp rax, 0x0
    je .succ
.loop:
    lea r9, [rdi+r8]     
    mov [r9], rax
    inc r8
    cmp r8, rsi
    ja .fail
    push rdi
    push rsi
    push r8
    call read_char
    pop r8
    pop rsi
    pop rdi
    cmp rax, 0x20
    je .succ
    cmp rax, 0x9
    je .succ
    cmp rax, 0xA
    je .succ
    cmp rax, 0x0
    je .succ
    jmp .loop
.succ:
    lea r9, [rdi+r8]     
    mov [r9], byte 0
    mov rax, rdi
    mov rdx, r8
    ret
.fail:
    mov rax, 0 
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    mov r8, 10
.loop:
    cmp byte [rdi], 0x30
    jb .end
    cmp byte [rdi], 0x39
    ja .end
    push rdx
    mul r8
    pop rdx
    add al, byte [rdi]
    sub al, 0x30    
    inc rdx
    inc rdi
    jmp .loop
.end:    
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    je .neg   
.pos:
    call parse_uint
    ret
.neg:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8
.loop: 
    cmp rax, rdx
    je .long
    mov r8b, [rdi+rax]
    mov byte [rsi+rax], r8b
    cmp r8b, 0
    je .end
    inc rax
    jmp .loop
.long:
    xor rax, rax
.end:
   ret
