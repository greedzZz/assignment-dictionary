%include "words.inc"
%include "lib.inc"

global _start

extern find_word

section .rodata
input_error: db "Incorrect input", 0
not_found_error: db "Word not found", 0

section .bss
buffer: times 255 resb 0

section .text

_start:
    mov rsi, 255
    mov rdi, buffer	
    call read_word
    cmp rdx, 0
    je .err_input
    mov rsi, PREV
    mov rdi, buffer
    call find_word
    cmp rax, 0
    je .err_not_found
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    inc rax
    add rdi, rax
    call print_string
    mov rdi, 0
    jmp .end
.err_input:
    mov rdi, input_error
    call print_error
    mov rdi, 1
    jmp .end
.err_not_found:
    mov rdi, not_found_error
    call print_error
    mov rdi, 1
.end:    
    jmp exit
