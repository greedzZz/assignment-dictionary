%include "colon.inc"

section .rodata

colon "three", third_word
db "third word explanation", 0

colon "two", second_word
db "second word explanation", 0 

colon "one", first_word
db "first word explanation", 0 
